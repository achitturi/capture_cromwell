## CPDLAB Capture Cromwell

WDL version of the [hybrid capture panel](https://gitlab.com/cpdlab/capture_dev) currently in clinical use at the CPD. Version 1.0 (HEME)

## Installation and Setup

#### Direct Access

Use your PMACS cluster credentials to log in to `consign.pmacs.upenn.edu`, and navigate to `/project/cpdlab/dev/akshay/cromwell`.

#### Repo Access

Clone the repository

    $ git clone https://gitlab.com/cpdlab/capture_cromwell.git

onto a Linux machine with Singularity version 3.7.1-1.el7 installed. For instructions on how to install Singularity on bare-metal Linux, [see here](https://docs.sylabs.io/guides/3.0/user-guide/installation.html).

Ensure you have access to the `$DATABASES` and `$FILES` paths in your local environment. These locations can be directly accessed on the PMACS central cluster.

    DATABASES="/project/cpdlab/dev/capture_dev/cap_databases"
    FILES="/project/cpdlab/clinical/capture_dev/files"

If you are running this pipeline in an environment outside of PMACS, nested tarballs of these directories are available upon request.

## Pipeline

From the top folder of the repo, copy governor.sh into the directory containing FastQ files you wish to process. 

    $ sh governor.sh

This script pulls a set of predefined Singularity images from DockerHub into the run directory and and separates each pair of FastQ files into its own folder. You may have to change the paths of certain variables to match those in your environment. Individual WDL files will be pulled into each individual sample directory, which will be allocated a node with 12 procs and 60 GB of memory using IBM's LSF job scheduler (you may skip/change this step to suit the needs of your own environment). The over-arching workflow is then launched.

    $ java -jar ${scripts}/cromwell-52.jar run capture-workflow.wdl -i all_inputs.json --options options.json

This launches `CaptureWorkflow`, which in turn spawns the following sub-workflows: `TrimTest`, `DedupAlign`, `VariantCalling`, `AnnotationTasks`, `VariantEffects`, `Parsing` and `CNV`. The ultimate output is a parsed TSV file of variants and a processed `.bam` file.

## Summarization

Also included in the repo is `summary-stats.wdl`, which runs on a created three-column TSV of sample inputs. Col1 is the samepl name, while Col2 and Col3 hold the locations of each sample .bam file and its index. This WDL workflow is needed for FileMaker statistics summarization (can be ignored otherwise), and spawns the following sub-workflows with sample scattering: `TotalReads`, `ReadsForAlignment`, `PerLost`, `PerMapped`, `OnTargetReads`, `PerOnTarget`, `MeanCov`, `CovArray`, `ExonicCov` and `WriteLines`. Output for each sample is then gathered at the run level with `SampleConcat`, resulting in a single run-level file of individual sample statistics.
