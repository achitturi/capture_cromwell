workflow parsing {
  File canonMAF_
  File smallvariantsMAF_
  call canonCut {
    input:
      canonMAF=canonMAF_,
      smallvariantsMAF=smallvariantsMAF_
  }
  call SVParse {
    input:
      finalMAF=canonCut.finalMAF
  }
  output {
      File parsedTSV=SVParse.parsedTSV
  }
}

task canonCut {
  File canonMAF
  File smallvariantsMAF
  String sample_name
  String lbrace = "{"
  String rbrace = "}"
  command { 
     canon_col=$(cut -f136 ${canonMAF} | awk -F',' "${lbrace}print $1${rbrace}") \
         && echo "$canon_col" > ${sample_name}.canon.tsv \
         && paste ${smallvariantsMAF} ${sample_name}.canon.tsv > ${sample_name}.final.smallvariants.maf
  }
  output {
      File canonTSV = "${sample_name}.canon.tsv"
      File finalMAF = "${sample_name}.final.smallvariants.maf"
  }
}

task SVParse {
  String sample_name
  File finalMAF
  String python_exec
  File legacy_parse
  command {
      ${python_exec} ${legacy_parse} ${finalMAF} ${sample_name}.parsed.tsv ${sample_name}
  }
  output {
      File parsedTSV = "${sample_name}.parsed.tsv"
  }
}
