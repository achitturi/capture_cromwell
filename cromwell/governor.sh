#!/bin/bash

## Governor. Catches all sample names in a run and
## launches processes for each. Stops upon error.

# Run script in run directory. Operates on bare
# FASTQ files.

scripts="/project/cpdlab/dev/akshay/cromwell"
test_dir="sample_directory"

export DATABASES="/project/cpdlab/dev/capture_dev/cap_databases"
export FILES="/project/cpdlab/clinical/capture_dev/files"
#export CACHE="/home/$USER/singularity-cachedir" # Set to /data1/ for Isilon tests.

# Pull down images with Singularity.
singularity pull docker://broadinstitute/gatk:4.1.0.0 # GATK 4.1.0.0
singularity pull docker://mskaccess/trim_galore # trim_galore v0.6.2. cutadapt
singularity pull docker://agrf/bwa-samtools:0.7.17.1.9 # bwa v0.7.17, samtools
singularity pull docker://mgibio/samtools-cwl:1.0.0 # samtools
singularity pull docker://ernfrid/bedtools:v2.27.1 # bedtools
singularity pull docker://singlecellpipeline/picard:v0.0.3 # picard
singularity pull shub://qbicsoftware/qbic-singularity-snpeff:latest # snpEff 4.3
singularity pull docker://dceoy/bcftools # bcftools
singularity pull docker://shuangbroad/pindel:v0.2.5b8 # pindel
singularity pull docker://mgibio/pindel2vcf-cwl:0.6.3 # pindel2vcf
singularity pull docker://centos/python-27-centos7 # python2.7
singularity pull docker://centos/python-36-centos7 # python3.6
singularity pull docker://m0zack/cnvkit:0.2 # cnvkit
rundir=$(pwd)

for file in $(ls *R1*.fastq.gz);
do
  prefix=$(echo ${file} | cut -d'-' -f1,2,3,4,5,6,7)
  sample_name=$(echo $(echo ${file} | cut -d'-' -f1,2,3,4,5,6,7)"-SEQ-"$(echo ${file} | cut -d'-' -f9 | cut -d'_' -f1))
  mkdir ${sample_name}
  mv ${prefix}*fastq.gz ${sample_name}/

  # Store operational script/workflow files in sample dir. Must replace individually
  cd ${sample_name}
  mv ${prefix}*R1*fastq.gz ${sample_name}_R1.fastq.gz
  mv ${prefix}*R2*fastq.gz ${sample_name}_R2.fastq.gz
  sample_dir=$(pwd)
  cp ${scripts}/*.wdl ${scripts}/*.json .
  sed -i "s|sample_string|${sample_name}|g" all_inputs.json
  sed -i "s|${test_dir}|${sample_dir}|g" all_inputs.json options.json
  sed -i "s|image_path|${rundir}|g" all_inputs.json

  # Once prepared, start job from the sample directory itself
  workflow="java -jar ${scripts}/cromwell-52.jar run capture-workflow.wdl -i all_inputs.json --options options.json"
  bsub -q cpd-dev -e error.log -o output.log -n 20 -M 75000 eval "$workflow"
  cd ../;
done
