workflow PindelVEP {
  File inputBAM_
  File inputBAMindex_
  String Pindel
  String Pindel2vcf
  File finalVCF_
  File canonVCF_
  call vcf2Maf {
    input:
      finalVCF=finalVCF_
  }
  call canon2Maf {
    input:
      canonVCF=canonVCF_
  }
  call mustCall {
    input:
      inputBAM=inputBAM_,
      inputBAMindex=inputBAMindex_
  }
  call createPindelConfig {
    input:
      inputBAM=inputBAM_,
      inputBAMindex=inputBAMindex_
  }
  call pindel {
    input:
      pindel_r=Pindel,
      inputBAM=inputBAM_,
      inputBAMindex=inputBAMindex_,
      PindelConfig=createPindelConfig.PindelConfig
  }
  call pindel2vcf {
    input:
      pindel2vcf_r=Pindel2vcf,
      pindel_BP=pindel.pindel_out_BP,
      pindel_CloseEndMapped=pindel.pindel_out_CloseEndMapped,
      pindel_D=pindel.pindel_out_D,
      pindel_INT_final=pindel.pindel_out_INT_final,
      pindel_INV=pindel.pindel_out_INV,
      pindel_LI=pindel.pindel_out_LI,
      pindel_RP=pindel.pindel_out_RP,
      pindel_SI=pindel.pindel_out_SI,
      pindel_TD=pindel.pindel_out_TD,
      inputBAM=inputBAM_,
      inputBAMindex=inputBAMindex_
  }
  call snpeffPindel {
    input:
      pindelVCF=pindel2vcf.pindelVCF
  }
  call vcf2vcfPindel {
    input:
      pindelVCF=snpeffPindel.snpEffPindelVCF
  }
  call vcf2mafPindel {
    input:
      pindelVCF=vcf2vcfPindel.PindelVCF
  }
  output {
      File outputMAF=vcf2Maf.outputMAF
      File canonMAF=canon2Maf.canonMAF
      File pindelMAF=vcf2mafPindel.PindelMAF
  }
}

task vcf2Maf {
    String sample_name
    String vcf2maf
    File finalVCF
    File perl
    File RefFasta
    File RefIndex
    File RefDict
    String vep
    File vep_cache
    command {
        ${perl} ${vcf2maf}/vcf2maf.pl \
            --input-vcf ${finalVCF} \
            --output-maf ${sample_name}.smallvariants.maf \
            --ref-fasta ${RefFasta} \
            --filter-vcf 0 \
            --vep-path ${vep} \
            --vep-data ${vep_cache} \
            --ncbi-build GRCh38 \
            --retain-info DP,MQ,ANN \
            --retain-fmt GT,AD,AF,DP,MAPPABILITY \
            --tumor-id ${sample_name}
    }
    output {
        File outputMAF = "${sample_name}.smallvariants.maf"
    }
}

task canon2Maf {
    String sample_name
    String vcf2maf
    File canonVCF
    File perl
    File RefFasta
    File RefIndex
    File RefDict
    String vep
    File vep_cache
    command {
        ${perl} ${vcf2maf}/vcf2maf.pl \
            --input-vcf ${canonVCF} \
            --output-maf ${sample_name}.canon.maf \
            --ref-fasta ${RefFasta} \
            --filter-vcf 0 \
            --vep-path ${vep} \
            --vep-data ${vep_cache} \
            --ncbi-build GRCh38 \
            --retain-info DP,MQ,ANN \
            --retain-fmt GT,AD,AF,DP,MAPPABILITY \
            --tumor-id ${sample_name}
    }
    output {
        File canonMAF = "${sample_name}.canon.maf"
    }
}

task mustCall {
    String sample_name
    String bcftoolsM
    String bcftoolsC
    String bcftoolsV
    File RefFasta
    File RefIndex
    File RefDict
    File MCL
    File inputBAM
    File inputBAMindex
    command {
        ${bcftoolsM} --threads 8 -Ou -R ${MCL} -f ${RefFasta} ${inputBAM} | ${bcftoolsC} -cMA -Ob -o ${sample_name}.bcf && ${bcftoolsV} ${sample_name}.bcf > ${sample_name}.mc.vcf
    }
    output {
        File mcVCF = "${sample_name}.mc.vcf"
    }
}

task createPindelConfig {
  String sample_name
  File inputBAM
  File inputBAMindex
  command {
        echo -e ${inputBAM}'\t'400'\t'${sample_name} > ${sample_name}.pindel.config
  }
  output {
        File PindelConfig = "${sample_name}.pindel.config"
  }
}

task pindel {
  String sample_name
  String pindel_r
  File RefFasta
  File PindelConfig
  File RefIndex
  File RefDict
  File inputBAM
  File inputBAMindex
  command {
      ${pindel_r} -T 8 \
        -f ${RefFasta} \
        -i ${PindelConfig} \
        -o ${sample_name}.pindel_out \
        --RP \
        -c chr13:28028291-28035934
  }
  output {
      File pindel_out_BP = "${sample_name}.pindel_out_BP"
      File pindel_out_CloseEndMapped = "${sample_name}.pindel_out_CloseEndMapped"
      File pindel_out_D = "${sample_name}.pindel_out_D"
      File pindel_out_INT_final = "${sample_name}.pindel_out_INT_final"
      File pindel_out_INV = "${sample_name}.pindel_out_INV"
      File pindel_out_LI = "${sample_name}.pindel_out_LI"
      File pindel_out_RP = "${sample_name}.pindel_out_RP"
      File pindel_out_SI = "${sample_name}.pindel_out_SI"
      File pindel_out_TD = "${sample_name}.pindel_out_TD"
  }
}

task pindel2vcf {
  String sample_name
  String pindel2vcf_r
  File RefFasta
  File RefIndex
  File RefDict
  File pindel_BP
  File pindel_CloseEndMapped
  File pindel_D
  File pindel_INT_final
  File pindel_INV
  File pindel_LI
  File pindel_RP
  File pindel_SI
  File pindel_TD
  File inputBAM
  File inputBAMindex
  command {
      ${pindel2vcf_r} -p ${pindel_TD} \
        -r ${RefFasta} \
        -R HG38 \
        -d 2020 \
        -v ${sample_name}.pindel.vcf
  }
  output {
      File pindelVCF = "${sample_name}.pindel.vcf"
  }
}

task snpeffPindel {
  String sample_name
  String snpEff
  File pindelVCF
  command {
      ${snpEff} -v -canon hg38 ${pindelVCF} \
        > ${sample_name}.pindel.snpEff.vcf
  }
  output {
      File snpEffPindelVCF = "${sample_name}.pindel.snpEff.vcf"
  }
}

task vcf2vcfPindel {
  String sample_name
  String vcf2maf
  File pindelVCF
  File perl
  File RefFasta
  File RefIndex
  File RefDict
  command {
      ${perl} ${vcf2maf}/vcf2vcf.pl \
        --input-vcf ${pindelVCF} \
        --output-vcf ${sample_name}.redone_pindel.snpEff.vcf \
        --ref-fasta ${RefFasta} \
        --retain-info ANN,HOMLEN \
        --retain-format GT,AD \
        --vcf-tumor-id ${sample_name}
  }
  output {
      File PindelVCF = "${sample_name}.redone_pindel.snpEff.vcf"
  }
}

task vcf2mafPindel {
  String sample_name
  String vcf2maf
  File pindelVCF
  File perl
  File RefFasta
  File RefIndex
  File RefDict
  String vep
  File vep_cache
  command {
      ${perl} ${vcf2maf}/vcf2maf.pl \
        --input-vcf ${pindelVCF} \
        --output-maf ${sample_name}.pindel.maf \
        --ref-fasta ${RefFasta} \
        --filter-vcf 0 \
        --vep-path ${vep} \
        --vep-data ${vep_cache} \
        --ncbi-build GRCh38 \
        --retain-info ANN,HOMLEN \
        --retain-fmt GT,AD \
        --tumor-id ${sample_name}
  }
  output {
      File PindelMAF = "${sample_name}.pindel.maf"
  }
}
