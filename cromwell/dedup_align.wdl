workflow taskDedupAlign {
    File inputBAM_
    File inputBAMindex_
    call markDuplicates {
      input:
        inputBAM=inputBAM_,
        inputBAMindex=inputBAMindex_
    }
    call addOrReplaceReadGroups {
      input:
        inputBAM=markDuplicates.outputBAM
    }
    call sortSam {
      input:
        inputBAM=addOrReplaceReadGroups.outputBAM
    }
    call cleanSam {
      input:
        inputBAM=sortSam.outputBAM
    }
    call baseRecal {
      input:
        inputBAM=cleanSam.outputBAM
    }
    call applyBQSR {
      input:
        inputBAM=cleanSam.outputBAM,
        recalTable=baseRecal.recalTable
    }
    output {
        File recalibratedBAM=applyBQSR.recalBAM
        File recalibratedBAMindex=applyBQSR.recalBAMindex
    }
}

task markDuplicates {
    String sample_name
    File inputBAM
    File inputBAMindex
    String picard
    command {
        ${picard} MarkDuplicates \
            TMP_DIR=./tmp \
            M=${sample_name}.dup.qc \
            I=${inputBAM} \
            O=${sample_name}.nodup.bam \
            REMOVE_DUPLICATES=true \
            VALIDATION_STRINGENCY=LENIENT
    }
    output {
        File outputBAM="${sample_name}.nodup.bam"
    }
}

task addOrReplaceReadGroups {
    String sample_name
    File inputBAM
    String picard
    command {
        ${picard} AddOrReplaceReadGroups \
            TMP_DIR=./tmp \
            I=${inputBAM} \
            O=${sample_name}.nodup.fix.bam \
            RGID=${sample_name} \
            RGLB="pennseq_heme" \
            RGPL=illumina \
            RGPU=unit1 \
            RGSM=${sample_name}
    }
    output {
        File outputBAM="${sample_name}.nodup.fix.bam"
    }
}

task sortSam {
    String sample_name
    File inputBAM
    String picard
    command {
        ${picard} SortSam \
            TMP_DIR=./tmp \
            I=${inputBAM} \
            O=${sample_name}.nodup.fix.srt.bam \
            SORT_ORDER=coordinate \
            CREATE_INDEX=true
    }
    output {
         File outputBAM="${sample_name}.nodup.fix.srt.bam"
    }
}

task cleanSam {
    String sample_name
    File inputBAM
    String picard
    command {
        ${picard} CleanSam \
            TMP_DIR=./tmp \
            I=${inputBAM} \
            O=${sample_name}.clean.bam \
            CREATE_INDEX=true
    }
    output {
        File outputBAM="${sample_name}.clean.bam"
    }
}

task baseRecal {
    String sample_name
    String mem
    File inputBAM
    File RefFasta
    File RefIndex
    File RefDict
    File RefAmb
    File RefAnn
    File RefInd
    File RefPac
    File RefBwt
    File RefAlt
    File MillsGold
    File MillsGoldIndex
    File dbSNP
    File dbSNPIndex
    command {
        gatk --java-options -Xmx${mem}m BaseRecalibrator \
            -R ${RefFasta} \
            --known-sites ${MillsGold} \
            --known-sites ${dbSNP} \
            -I ${inputBAM} \
            -O ${sample_name}.recal_data.table
    }
    output {
        File recalTable = "${sample_name}.recal_data.table"
    }
}

task applyBQSR {
    String sample_name
    String mem
    File inputBAM
    File recalTable
    File RefFasta
    File RefIndex
    File RefDict
    File RefAmb
    File RefAnn
    File RefInd
    File RefPac
    File RefBwt
    File RefAlt
    command {
        gatk --java-options -Xmx${mem}m ApplyBQSR \
        -R ${RefFasta} \
        -I ${inputBAM} \
        --bqsr-recal-file ${recalTable} \
        -O ${sample_name}.recal.bam
    }
    output {
        File recalBAM = "${sample_name}.recal.bam"
        File recalBAMindex = "${sample_name}.recal.bai"
    }
}
