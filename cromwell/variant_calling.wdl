workflow taskVariantCalling {
    File inputBAM_
    File inputBAMindex_
    call indexBAM {
      input:
        recalBAM=inputBAM_
    }
    call collectSequencingArtifactMetrics {
      input:
        inputBAM=inputBAM_,
        inputBAMindex=inputBAMindex_
    }
    call getPileupSummaries {
      input:
        inputBAM=inputBAM_,
        inputBAMindex=inputBAMindex_
    }
    call calculateContamination {
      input:
        pileup=getPileupSummaries.pileup
    }
    call haplotypeCaller {
      input:
        inputBAM=inputBAM_,
        inputBAMindex=inputBAMindex_
    }
    call selectVariants {
      input:
        hapVCF=haplotypeCaller.haplotypeVCF,
        hapVCFindex=haplotypeCaller.haplotypeVCFindex
    }
    call mutect {
      input:
        inputBAM=inputBAM_,
        inputBAMindex=inputBAMindex_
    }
    call filterMutectCalls {
      input:
        ContamTable=calculateContamination.contamination,
        rawVCF=mutect.mutectVCF,
        rawVCFindex=mutect.mutectVCFindex
    }
    call filterByOrientationBias {
      input:
        filterVCF=filterMutectCalls.filteredVCF,
        filterVCFindex=filterMutectCalls.filteredVCFindex,
        adaptMetrics=collectSequencingArtifactMetrics.adapterMetrics
    }
    call mergeVcfs {
      input:
        somaticVCF=filterByOrientationBias.finalFilteredVCF,
        somaticVCFindex=filterByOrientationBias.finalFilteredVCFindex,
        germlineVCF=selectVariants.haploVCF,
        germlineVCFindex=selectVariants.haploVCFindex
    }
    output {
        File svVCF=mergeVcfs.smallvariantsVCF
        File svVCFindex=mergeVcfs.smallvariantsVCFindex
        File BAMindex=inputBAMindex_
    }
}

task indexBAM {
    String sample_name
    File recalBAM
    command {
        samtools index ${recalBAM} -b ${sample_name}.recal.bam.bai
    }
    output {
        File inputBAMindex = "${sample_name}.recal.bam.bai"
    }
}

task collectSequencingArtifactMetrics {
    String sample_name
    String mem
    File RefFasta
    File RefIndex
    File RefDict
    File inputBAM
    File inputBAMindex
    command {
        gatk --java-options -Xmx${mem}m CollectSequencingArtifactMetrics \
            -I ${inputBAM} \
            -O ${sample_name} \
            --FILE_EXTENSION .txt \
            -R ${RefFasta}
    }
    output {
        File adapterMetrics = "${sample_name}.pre_adapter_detail_metrics.txt"
    }
}

task getPileupSummaries {
    String sample_name
    String mem
    File RefFasta
    File RefIndex
    File RefDict
    File gnomad
    File gnomadindex
    File targetBed
    File inputBAM
    File inputBAMindex
    command {
        gatk --java-options -Xmx${mem}m GetPileupSummaries \
            -I ${inputBAM} \
            -O ${sample_name}.targeted_sequencing.table \
            -V ${gnomad} \
            -R ${RefFasta} \
            -L ${targetBed}
    }
    output {
        File pileup = "${sample_name}.targeted_sequencing.table"
    }
}

task calculateContamination {
    String sample_name
    String mem
    File pileup
    command {
        gatk --java-options -Xmx${mem}m CalculateContamination \
            -I ${pileup} \
            -O ${sample_name}.targeted_sequencing.contamination.table
    }
    output {
        File contamination = "${sample_name}.targeted_sequencing.contamination.table"
    }
}

task haplotypeCaller {
    String sample_name
    String mem
    File RefFasta
    File RefIndex
    File RefDict
    File inputBAM
    File inputBAMindex
    command {
        gatk --java-options -Xmx${mem}m HaplotypeCaller \
            -R ${RefFasta} \
            -I ${inputBAM} \
            -O ${sample_name}.haplotypecaller.vcf.gz
    }
    output {
        File haplotypeVCF = "${sample_name}.haplotypecaller.vcf.gz"
        File haplotypeVCFindex = "${sample_name}.haplotypecaller.vcf.gz.tbi"
    }
}

task selectVariants {
    String sample_name
    String mem
    File RefFasta
    File RefIndex
    File RefDict
    File hapVCF
    File hapVCFindex
    File targetBed
    command {
        gatk --java-options -Xmx${mem}m SelectVariants \
        -R ${RefFasta} \
        -V ${hapVCF} \
        -O ${sample_name}.haplo.vcf.gz \
        -L ${targetBed}
    }
    output {
        File haploVCF="${sample_name}.haplo.vcf.gz"
        File haploVCFindex="${sample_name}.haplo.vcf.gz.tbi"
    }
}

task mutect {
    String sample_name
    String mem
    File RefFasta
    File RefIndex
    File RefDict
    File inputBAM
    File inputBAMindex
    File gnomad
    File gnomadindex
    File capturePON
    File capturePONindex
    command {
        gatk --java-options -Xmx${mem}m Mutect2 \
            -R ${RefFasta} \
            -I ${inputBAM} \
            -tumor ${sample_name} \
            --minimum-mapping-quality 0 \
            --germline-resource ${gnomad} \
            -pon ${capturePON} \
            -O ${sample_name}.mutect.vcf.gz
    }
    output {
        File mutectVCF = "${sample_name}.mutect.vcf.gz"
        File mutectVCFindex = "${sample_name}.mutect.vcf.gz.tbi"
    }
}

task filterMutectCalls {
    String sample_name
    String mem
    File rawVCF
    File rawVCFindex
    File ContamTable
    File targetBed
    command {
        gatk --java-options -Xmx${mem}m FilterMutectCalls \
            -O ${sample_name}.filtered.vcf.gz \
            -V ${rawVCF} \
            --contamination-table ${ContamTable} \
            -L ${targetBed}
    }
    output {
        File filteredVCF = "${sample_name}.filtered.vcf.gz"
        File filteredVCFindex = "${sample_name}.filtered.vcf.gz.tbi"
    }

}

task filterByOrientationBias {
    String sample_name
    String mem
    File filterVCF
    File filterVCFindex
    File adaptMetrics
    command {
      gatk --java-options -Xmx${mem}m FilterByOrientationBias \
          -V ${filterVCF} \
          --artifact-modes 'G/T' \
          --artifact-modes 'C/T' \
          -P ${adaptMetrics} \
          -O ${sample_name}.final.filtered.vcf.gz
    }
    output {
      File finalFilteredVCF = "${sample_name}.final.filtered.vcf.gz"
      File finalFilteredVCFindex = "${sample_name}.final.filtered.vcf.gz.tbi"
    }
}

task mergeVcfs {
    String sample_name
    String mem
    String picard
    File somaticVCF
    File somaticVCFindex
    File germlineVCF
    File germlineVCFindex
    command {
      ${picard} MergeVcfs \
          I=${somaticVCF} \
          I=${germlineVCF} \
          O=${sample_name}.smallvariants.vcf.gz
    }
    output {
        File smallvariantsVCF = "${sample_name}.smallvariants.vcf.gz"
        File smallvariantsVCFindex = "${sample_name}.smallvariants.vcf.gz.tbi"
    }
}
