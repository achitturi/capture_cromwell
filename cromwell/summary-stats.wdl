workflow SummaryStats {
  File inputSamplesFile
  Array[Array[File]] sampleBAMs = read_tsv(inputSamplesFile)

  scatter (sample in sampleBAMs) {
    call TotalReads {
      input:
        inputBAM=sample[1],
        inputBAMindex=sample[2]
    }
    call ReadsForAlignment {
      input:
        inputBAM=sample[1],
        inputBAMindex=sample[2]
    }
    call PerLost {
      input:
        total_reads=TotalReads.total_reads,
        reads_for_alnment=ReadsForAlignment.reads_for_alnment
    }
    call PerMapped {
      input:
        total_reads=TotalReads.total_reads,
        reads_for_alnment=ReadsForAlignment.reads_for_alnment
    }
    call OnTargetReads {
      input:
        inputBAM=sample[1],
        inputBAMindex=sample[2]
    }
    call PerOnTarget {
      input:
        on_target_reads=OnTargetReads.on_target_reads,
        total_reads=TotalReads.total_reads
    }
    call MeanCov {
      input:
        inputBAM=sample[1],
        inputBAMindex=sample[2]
    }
    call CovArray {
      input:
        inputBAM=sample[1],
        inputBAMindex=sample[2]
    }
    call ExonicCov {
      input:
        inputBAM=sample[1],
        inputBAMindex=sample[2]
    }
    call WriteLines {
      input:
        name=sample[0],
        inputBAM=sample[1],
        inputBAMindex=sample[2],
        total_reads=TotalReads.total_reads,
        reads_for_alnment=ReadsForAlignment.reads_for_alnment,
        per_lost=PerLost.per_lost,
        per_mapped=PerMapped.per_mapped,
        on_target_reads=OnTargetReads.on_target_reads,
        per_on_target=PerOnTarget.per_on_target,
        mean_cov=MeanCov.mean_cov,
        per_250=CovArray.per_250,
        per_1K=CovArray.per_1K,
        below250=ExonicCov.below250,
        below150=ExonicCov.below150
    }
    output {
        Array[File] test = WriteLines.test
        Array[File] sampleMVT = WriteLines.sampleMVT
    }
  }
  call SampleConcat {
    input:
      Stats=WriteLines.sampleMVT
  }
  output {
    File RSF = SampleConcat.RSF
  }
}

task TotalReads {
  File inputBAM
  File inputBAMindex
  String samtools
  command {
      ${samtools} view -@ 9 -c ${inputBAM} > total_reads.txt
  }
  output {
      File total_reads = "total_reads.txt"
  }
}

task ReadsForAlignment {
  File inputBAM
  File inputBAMindex
  String samtools
  command {
      ${samtools} view -@ 9 -c -F 260 ${inputBAM} > reads_for_alnment.txt
  }
  output {
      File reads_for_alnment = "reads_for_alnment.txt"
  }
}

task PerLost {
  File total_reads
  File reads_for_alnment
  String bc
  command {
      echo "scale=4; 100-($(cat ${reads_for_alnment})/$(cat ${total_reads}))*100" | ${bc} \
        > per_lost.txt
  }
  output {
      File per_lost = "per_lost.txt"
  }
}

task PerMapped {
  File total_reads
  File reads_for_alnment
  String bc
  command {
      echo "scale=4; ($(cat ${reads_for_alnment})/$(cat ${total_reads}))*100" | ${bc} \
        > per_mapped.txt
  }
  output {
      File per_mapped = "per_mapped.txt"
  }
}

task OnTargetReads {
  File inputBAM
  File inputBAMindex
  File bedfile
  String samtools
  command {
      ${samtools} view -@ 9 -c -F 260 -L ${bedfile} ${inputBAM} > on_target_reads.txt
  }
  output {
      File on_target_reads = "on_target_reads.txt"
  }
}

task PerOnTarget {
  File total_reads
  File on_target_reads
  String bc
  command {
      echo "scale=4; ($(cat ${on_target_reads})/$(cat ${total_reads}))*100" | ${bc} \
        > per_on_target.txt
  }
  output {
      File per_on_target = "per_on_target.txt"
  }
}

task MeanCov {
  File inputBAM
  File inputBAMindex
  File heme_bed
  String samtools
  String lbrace = "{"
  String rbrace = "}"
  command {
      ${samtools} depth -m 1000000 -b ${heme_bed} ${inputBAM} \
        | awk '${lbrace}sum+=$3${rbrace} END ${lbrace}print sum/NR${rbrace}' > mean_cov.txt
  }
  output {
      File mean_cov = "mean_cov.txt"
  }
}

task CovArray {
  File inputBAM
  File inputBAMindex
  File bedfile
  String samtools
  String bc
  String lbrace = "{"
  String rbrace = "}"
  command {
      ${samtools} depth -m 1000000 -b ${bedfile} ${inputBAM} \
        | awk '${lbrace}print $3${rbrace}' > cov_array.txt \
      && echo "scale=4; (1-$(cat cov_array.txt | awk '$0<1' | wc -l)/$(cat cov_array.txt | wc -l)
)*100" | ${bc} > per_0.txt \
      && echo "scale=4; (1-$(cat cov_array.txt | awk '$0<2' | wc -l)/$(cat cov_array.txt | wc -l))*100" | ${bc} > per_1.txt \
      && echo "scale=4; (1-$(cat cov_array.txt | awk '$0<250' | wc -l)/$(cat cov_array.txt | wc -l))*100" | bc > per_250.txt \
      && echo "scale=4; (1-$(cat cov_array.txt | awk '$0<1000' | wc -l)/$(cat cov_array.txt | wc -l))*100" | bc > per_1K.txt \
  }
  output {
      File cov_array = "cov_array.txt"
      File per_0 = "per_0.txt"
      File per_1 = "per_1.txt"
      File per_250 = "per_250.txt"
      File per_1K = "per_1K.txt"
  }
}

task ExonicCov {
  File inputBAM
  File inputBAMindex
  File target_bed
  String samtools
  String lbrace = "{"
  String rbrace = "}"
  command {
      ${samtools} bedcov ${target_bed} ${inputBAM} > target_bedcov.tsv \
      && cat target_bedcov.tsv | awk '$8<250' | wc -l > regions_below250.txt \
      && cat target_bedcov.tsv | awk '$8<150' | wc -l > regions_below150.txt
  }
  output {
      File below250 = "regions_below250.txt"
      File below150 = "regions_below150.txt"
  }
}

task WriteLines {
  File inputBAM
  File inputBAMindex
  File total_reads
  File reads_for_alnment
  File per_lost
  File per_mapped
  File on_target_reads
  File per_on_target
  File mean_cov
  File per_250
  File per_1K
  File below250
  File below150
  String name
  command {
      cat ${total_reads} > ${name}.test.txt && \
      echo "${name} $(cat ${total_reads}) $(cat ${reads_for_alnment}) $(cat ${per_lost}) $(cat ${reads_for_alnment}) $(cat ${per_mapped}) $(cat ${on_target_reads}) $(cat ${per_on_target}) $(cat ${on_target_reads}) NA $(cat ${per_on_target}) $(cat ${mean_cov}) NA NA $(cat ${per_250}) $(cat ${per_1K}) $(cat ${below250}) NA $(cat ${below150}) TBA TBA" > ${name}.RunStatsFinal.txt; sed -i "s| |$(printf '\t')|g" ${name}.RunStatsFinal.txt
  }
  output {
      File sampleMVT = "${name}.RunStatsFinal.txt"
      File test = "${name}.test.txt"
  }
}

task SampleConcat {
  String sample_name
  Array[File] Stats
  command {
      cat ${sep=" " Stats} >> ${sample_name}.RunStatsFinal.txt
  }
  output {
      File RSF = "${sample_name}.RunStatsFinal.txt"
  }
}
