import "trim_test.wdl" as TrimTest
import "dedup_align.wdl" as DedupAlign
import "variant_calling.wdl" as VariantCalling
import "annotation.wdl" as AnnotationTasks
import "VEP.wdl" as VariantEffects
import "parsing.wdl" as Parsing
import "CNV.wdl" as CNV
import "summary-stats.wdl" as SummaryStats

workflow CaptureWorkflow {
  call TrimTest.taskFastqProc
  call DedupAlign.taskDedupAlign {
    input:
      inputBAM_=taskFastqProc.aligned_bam,
      inputBAMindex_=taskFastqProc.aligned_bam_index
  }
  call VariantCalling.taskVariantCalling {
    input:
      inputBAM_=taskDedupAlign.recalibratedBAM,
      inputBAMindex_=taskDedupAlign.recalibratedBAMindex
  }
  call AnnotationTasks.variantAnnotation {
    input:
      inputVCF_=taskVariantCalling.svVCF,
      inputVCFindex_=taskVariantCalling.svVCFindex
  }
  call VariantEffects.PindelVEP {
    input:
      inputBAM_=taskDedupAlign.recalibratedBAM,
      inputBAMindex_=taskVariantCalling.BAMindex,
      finalVCF_=variantAnnotation.finalVCF,
      canonVCF_=variantAnnotation.canonVCF
  }
  call Parsing.parsing {
    input:
      smallvariantsMAF_=PindelVEP.outputMAF,
      canonMAF_=PindelVEP.canonMAF
  }
  call CNV.CopyNumber {
    input:
      inputBAM_=taskDedupAlign.recalibratedBAM,
      inputBAMindex_=taskVariantCalling.BAMindex,
  }
  output {
    File outputBAMs=taskDedupAlign.recalibratedBAM
    File outputBAMindexes=taskDedupAlign.recalibratedBAMindex
    File parsedTSV=parsing.parsedTSV
  }
}
