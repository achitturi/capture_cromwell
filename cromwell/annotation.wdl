workflow variantAnnotation {
  File inputVCF_
  File inputVCFindex_
  call leftAlignTrimVariants {
    input:
      inputVCF=inputVCF_,
      inputVCFindex=inputVCFindex_
  }
  call snpEffAnnotation {
    input:
      alignedVCF=leftAlignTrimVariants.alignedVCF
  }
  call canonAnnotation {
    input:
      alignedVCF=leftAlignTrimVariants.alignedVCF
  }
  call mappability {
    input:
      snpeffVCF=snpEffAnnotation.snpEffVCF
  }
  output {
      File finalVCF=mappability.finalVCF
      File canonVCF=canonAnnotation.canonVCF
  }
}

task leftAlignTrimVariants {
    String sample_name
    File inputVCF
    File inputVCFindex
    File RefFasta
    File RefIndex
    File RefDict
    File RefAmb
    File RefAnn
    File RefInd
    File RefPac
    File RefBwt
    File RefAlt
    command {
        gatk LeftAlignAndTrimVariants \
            --reference ${RefFasta} \
            --variant ${inputVCF} \
            --output ${sample_name}.smallvariants.aligned.vcf.gz \
            --split-multi-allelics
    }
    output {
        File alignedVCF = "${sample_name}.smallvariants.aligned.vcf.gz"
    }
}

task snpEffAnnotation {
    String sample_name
    String snpEff
    File alignedVCF
    command {
        ${snpEff} -v -o vcf hg38 ${alignedVCF} \
            > ${sample_name}.snpEff.smallvariants.vcf.gz
    }
    output {
        File snpEffVCF = "${sample_name}.snpEff.smallvariants.vcf.gz"
    }
}

task canonAnnotation {
    String sample_name
    String snpEff
    File alignedVCF
    command {
        ${snpEff} -v -canon hg38 ${alignedVCF} \
            > ${sample_name}.canon.vcf
    }
    output {
        File canonVCF = "${sample_name}.canon.vcf"
    }
}

task mappability {
    String sample_name
    File header
    File map_bed
    File map_bed_gz
    File map_bed_tbi
    File snpeffVCF
    command {
        bcftools annotate -a ${map_bed}.gz -h ${header} \
            -c CHROM,FROM,TO,FMT/MAPPABILITY ${snpeffVCF} \
            -o ${sample_name}.final.smallvariants.vcf
    }
    output {
        File finalVCF = "${sample_name}.final.smallvariants.vcf"
    }
}
