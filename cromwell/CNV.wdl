workflow CopyNumber {
  File inputBAM_
  File inputBAMindex_
  call batchStart {
    input:
      inputBAM=inputBAM_,
      inputBAMindex=inputBAMindex_
  }
}

task batchStart {
  String cnvkit
  File inputBAM
  File inputBAMindex
  File norm_ref
  String sample_name
  command {
    ${cnvkit} batch ${inputBAM} -r ${norm_ref} --drop-low-coverage
  }
  output {
    File finalCNS = "${sample_name}.cns"
    File finalCNR = "${sample_name}.cnr"
  }
}
