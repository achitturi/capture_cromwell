workflow taskFastqProc {
    call trimGalore
    call bwaMEM {
      input:
        trim_fq1=trimGalore.trimmed1,
        trim_fq2=trimGalore.trimmed2
    }
    call flagStat {
      input:
        out_sam=bwaMEM.out_sam
    }
    call produceBAM {
      input:
        out_sam=bwaMEM.out_sam
    }
    call indexBAM {
      input:
        align_bam=produceBAM.align_bam
    }
    output {
        File aligned_bam=produceBAM.align_bam
        File aligned_bam_index=indexBAM.align_bai
    }
}

task trimGalore {
    String trim_galore
    String sample_name
    File inputFQ1
    File inputFQ2
    String home_dir
    command {
        ${trim_galore} --illumina --fastqc \
            --paired ${inputFQ1} ${inputFQ2} \
            --path_to_cutadapt cutadapt
    }
    output {
        File trimmed1 = "${sample_name}_R1_val_1.fq.gz"
        File trimmed2 = "${sample_name}_R2_val_2.fq.gz"
    }
}

task bwaMEM {
    String bwa_mem
    String sample_name
    String cpu
    File RefFasta
    File RefIndex
    File RefDict
    File RefAmb
    File RefAnn
    File RefInd
    File RefPac
    File RefBwt
    File RefAlt
    File trim_fq1
    File trim_fq2
    command {
        ${bwa_mem} -t ${cpu} \
            -K 100000000 \
            ${RefFasta} \
            ${trim_fq1} ${trim_fq2} \
            > ${sample_name}.align.sam
    }
    output {
        File out_sam = "${sample_name}.align.sam"
    }
}

task flagStat {
  String samtools_flagstat
  String sample_name
  File out_sam
  command {
      ${samtools_flagstat} ${out_sam} > ${sample_name}.align.stat
  }
  output {
    File align_stat = "${sample_name}.align.stat"
  }
}

task produceBAM {
  String samtools_view
  String sample_name
  File out_sam
  command {
      ${samtools_view} -Su ${out_sam} | samtools sort -m 30000000000 -o ${sample_name}.align.bam
  }
  output {
      File align_bam = "${sample_name}.align.bam"
  }
}

task indexBAM {
  String samtools_index
  String sample_name
  File align_bam
  command {
      ${samtools_index} ${align_bam} -b ${sample_name}.align.bam.bai
  }
  output {
      File align_bai = "${sample_name}.align.bam.bai"
  }
}
